// Ex 8.1. NSDMI Basics. (非静态数据初始化)

#include <iostream>
#include <numeric>

size_t calcCheckSum( std::string const& s )
{
  return std::accumulate( s.begin(), s.end(), 0ul );
}

class DataPacket
{
public:
  DataPacket() = default;

  DataPacket( std::string const& data, size_t serverId )
      : data_ { data }, checkSum_ { calcCheckSum( data ) }, serverId_ { serverId }
  {
  }

  std::string const& getData() const
  {
    return data_;
  }
  void setData( std::string const& data )
  {
    data_ = data;
    checkSum_ = calcCheckSum( data );
  }
  size_t getCheckSum() const
  {
    return checkSum_;
  }

  void setServerId( size_t id )
  {
    serverId_ = id;
  }
  size_t getServerId() const
  {
    return serverId_;
  }

private:
  std::string data_;
  size_t checkSum_ { 0 };
  size_t serverId_ { 0 };
};

int main()
{
  DataPacket dp;
}
