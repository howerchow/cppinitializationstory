// Ex 10.2. A raw pointer as a data member

// In summary, a class type with a raw pointer non-static data member has the following
// properties:
// • It will be default constructible, but it’s best to assign some starting value to the pointer
// (or at least nullptr).
// • The compiler can generate a copy and move constructors, but it will be a shallow copy!
// • The compiler can create default copy assignment and move assignment operators, but
// the operations will also be shallow!

#include <iostream>
#include <string>

class DangerousWrapper
{
public:
  explicit DangerousWrapper( std::string* pstr ) : pName_ { pstr } {}
  std::string* name() const
  {
    return pName_;
  }
  void name( std::string* pstr )
  {
    pName_ = pstr;
  }

private:
  std::string* pName_ { nullptr };
};

int main()
{
  std::string str { "Name" };
  DangerousWrapper w { &str };
  DangerousWrapper x { w }; // Shallow Copy
  std::cout << *w.name() << '\n'; // "Name" !!!
  *( x.name() ) = "Other";
  std::cout << *w.name() << '\n'; // "Other" !!!
}
