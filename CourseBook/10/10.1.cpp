// Ex 10.1. A constant data member.

// When one data member is const, the compiler won’t generate a default
// assignment operator for us.

// If you want your object to be constant, make it const as a “whole” rather
// than just some of its parts.

// We can summarize a class type with a const non-static data member as:
// • It will be default constructible only when you assign a default value to the member
// (NSDMI); otherwise, it won’t be default constructible.
// • The compiler can generate a copy and move constructors.
// • Default copy assignment and move assignment operators are blocked.

#include <iostream>
#include <string>
#include <vector>

class ProductConst
{
public:
  explicit ProductConst( const char* name, unsigned id ) : name_ { name }, id_ { id } {}
  std::string const& name() const
  {
    return name_;
  }
  void name( const std::string& name )
  {
    name_ = name;
  }
  unsigned id() const
  {
    return id_;
  }

private:
  std::string name_;
  const unsigned id_;
};

int main()
{
  ProductConst tvset { "TV Set", 123 };
  std::cout << tvset.name() << ", id: " << tvset.id() << '\n';
  ProductConst copy { tvset }; // work fine: copy construction
  std::cout << copy.name() << ", id: " << copy.id() << '\n';

  std::vector<ProductConst> prods {};
  prods.push_back( ProductConst { "box", 234 } ); // work fine: move construction
  prods.push_back( ProductConst { "car", 567 } ); // work fine
  // error: use of deleted function ‘ProductConst&
  // ProductConst::operator=(ProductConst&&)’
  // prods.insert( prods.begin(), ProductConst{ "ball", 987 } );
}
