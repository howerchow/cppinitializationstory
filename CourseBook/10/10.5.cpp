// Ex 10.5. A shared pointer as a data member.

// shared_ptr has different semantics. Rather than
// restricting the resource to a single owner, shared_ptr works with several owners that share
// a single resource.

#include <algorithm>
#include <iostream>
#include <memory>

struct Value
{
  explicit Value( int v ) : v_( v )
  {
    std::cout << "Value(" << v_ << ")\n";
  }
  ~Value() noexcept
  {
    std::cout << "~Value(" << v_ << ")\n";
  }
  int v_;
};

class ProductWithSharedPtr
{
public:
  ProductWithSharedPtr() = default;
  explicit ProductWithSharedPtr( const char* name, std::shared_ptr<Value> pId ) : name_ { name }, pId_ { pId } {}
  std::string const& name() const
  {
    return name_;
  }
  int id() const
  {
    return pId_ ? pId_->v_ : 0;
  }

private:
  std::string name_;
  std::shared_ptr<Value> pId_;
};

int main()
{
  auto pId = std::make_shared<Value>( 123 );
  ProductWithSharedPtr tv { "TV Set", pId };
  std::cout << "tv: " << tv.name() << ", id: " << tv.id() << '\n';
  std::cout << "pId use count: " << pId.use_count() << '\n';
  {
    ProductWithSharedPtr copy { tv };
    std::cout << "tv: " << tv.name() << ", id: " << tv.id() << '\n';
    std::cout << "copy: " << copy.name() << ", id: " << copy.id() << '\n';
    pId->v_ = 100;
    std::cout << "tv: " << tv.name() << ", id: " << tv.id() << '\n';
    std::cout << "copy: " << copy.name() << ", id: " << copy.id() << '\n';
    std::cout << "pId use count: " << pId.use_count() << '\n';
  }
  std::cout << "pId use count: " << pId.use_count() << '\n';
}

// Summary for smart pointers
// In summary, a class type with a smart pointer non-static data member has the following
// properties:
// • It will be default constructible, but it’s best to assign some starting value to the pointer
// (or at least nullptr).
// • The compiler can generate a move constructor and move assignment operator.
// • For unique_ptr default copy operations are blocked, and you must implement custom
// versions.
// • For shared_ptr, default copy operations are provided, but they are “shallow”. This
// is still safer than copying raw pointers, as this time, we copy shared pointers which
// increases their internal reference counter, and thus the resource handling will be safe
// (although it might be harder to reason about).
