// Ex 13.1. Strong types and area units classes.

constexpr double ToWattsRatio { 745.699872 };
class HorsePower;

class WattPower
{
public:
  WattPower() = default;
  explicit WattPower( double p ) : power_ { p } {}
  explicit WattPower( HorsePower const& h );
  double getValue() const
  {
    return power_;
  }

private:
  double power_ { 0. };
};

class HorsePower
{
public:
  HorsePower() = default;
  explicit HorsePower( double p ) : power_ { p } {}
  explicit HorsePower( const WattPower& w );
  double getValue() const
  {
    return power_;
  }

private:
  double power_ { 0. };
};
