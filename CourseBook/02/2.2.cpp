// Ex 2.12. Implicit Conversions.

// Converting Constructors

#include <iostream>
#include <string>

struct Product
{
  Product() : name { "default product" }, value {} {}
  explicit Product( double v ) : name { "basic" }, value { v } {}
  Product( const std::string& n, double v ) : name { n }, value { v } {}

  std::string name;
  double value;
};

void printProduct( Product const& prod )
{
  std::cout << prod.name << ", " << prod.value << '\n';
}

int main()
{
  double someRandomNumber = 100.0;
  printProduct( Product { someRandomNumber } );
  printProduct( { "a box", 2 } );
}
