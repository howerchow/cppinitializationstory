// The double life of objects

#include <cassert>
#include <iostream>

class Rng
{
  int _min;
  int _max;
  // invariant: _min <= _max

public:
  Rng( int lo, int hi )
      // precondition: lo <= hi
      : _min( lo ), _max( hi )
  {
    assert( _min <= _max );
  }

  int min() const
  {
    return _min;
  }
  int max() const
  {
    return _max;
  }

  void set( int lo, int hi )
  // precondition: lo <= hi
  {
    _min = lo;
    _max = hi;
    assert( _min <= _max );
  }
};

const Rng foo()
{
  const Rng x { 1, 2 };
  return x;
}

Rng bar()
{
  Rng y = foo();
  y.set( 3, 4 );
  return y;
}

int main()
{
  const Rng z = bar();
  std::cout << z.min() << " " << z.max() << std::endl;
}
