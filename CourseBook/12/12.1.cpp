// Ex 12.1. Aggregate classes, several examples.

// An aggregate is an array or a class type with:
// • no user-provided, explicit, or inherited constructors
// • no private or protected non-static data members
// • no virtual functions, and
// • no virtual, private, or protected base classes

#include <iostream>
#include <memory>
#include <type_traits>
#include <vector>

struct Base
{
  int x { 42 };
};
struct Derived : Base
{
  int y;
};

struct Param
{
  std::string name;
  int val;
  void Parse(); // member functions allowed
};

struct Point
{
  int x;
  int y;

  // Point(Point &&) = delete;
};

template <typename T>
struct Aggregate
{
  T t;
};

class AggregateClass
{
public:
  Point p;

private:
  void f() {}
};

int main()
{
  // std::cout << std::is_aggregate_v<Derived> << std::endl;
  // std::cout << std::is_aggregate_v<Param> << std::endl;
  // std::cout << std::is_aggregate_v<Point> << std::endl;

  // auto ptr = std::make_unique<Point>(10, 20); // works fine C++20

  // std::vector<Point> points;
  // points.emplace_back(10, 20); // works fine C++20

  Aggregate b { 1 }; // ok since C++20*
  AggregateClass AC {1, 2};

}
