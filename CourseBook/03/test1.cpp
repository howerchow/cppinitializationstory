// Guaranteed copy elision

#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <utility>
#include <vector>

// ------------------------------------------------------
struct NonMovable
{
  NonMovable() noexcept
  {
    std::cout << "NonMovable()" << std::endl;
  }
  NonMovable( NonMovable&& ) noexcept = delete;
  NonMovable& operator=( NonMovable&& ) noexcept = delete;
};

NonMovable MakeNonMovable()
{
  return NonMovable {};
}

// ----------------------------------------------------
struct Movable
{
  Movable() noexcept
  {
    std::cout << "Movable()" << std::endl;
  }
  Movable( Movable const& )
  {
    std::cout << "Movable(Movable const&)" << std::endl;
  }
  Movable& operator=( Movable& )
  {
    std::cout << "Movable& operator=(Movable &&)" << std::endl;
    return *this;
  }
  Movable( Movable&& ) noexcept
  {
    std::cout << "Movable(Movable &&)" << std::endl;
  }
  Movable& operator=( Movable&& ) noexcept
  {
    std::cout << "Movable& operator=(Movable &&)" << std::endl;
    return *this;
  }
  ~Movable()
  {
    std::cout << "~Movable()" << std::endl;
  };
};

Movable MakeMovable()
{
  return Movable {};
}

int main()
{

  // -------------------------Copy Elision------------------------
  // [[maybe_unused]] const auto x = NonMovable{};
  // [[maybe_unused]] const auto z = NonMovable{};
  // [[maybe_unused]] const auto y = NonMovable{NonMovable{}};

  // ------------------------Move Semantics-----------------------
  // Movable m1{};
  // std::cout << "--------------" << std::endl;
  // m1 = MakeMovable(); // move assignmnet
  // std::cout << "++++++++++++++" << std::endl;
  // Movable m2{};
  // m2 = std::move(m1); // move assignmnet

  // std::string s {"hello"};
  // std::vector<std::string> v{};
  // v.push_back(std::move(s));
  // std::cout << s << std::endl;

  // -----------------------------------------------------------
  auto it = std::make_unique<Movable>();
  std::list<std::unique_ptr<Movable>> l {};
  l.emplace_back( std::move( it ) );
  std::printf( "++++++++++++\n" );
  l.pop_back();
}
