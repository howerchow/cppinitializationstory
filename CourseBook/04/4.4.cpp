// Ex 4.4. Inheriting constructors (继承构造)

#include <iostream>
#include <numeric>

size_t calcCheckSum( std::string const& s )
{
  return std::accumulate( s.begin(), s.end(), 0ul );
}

class DataPacket
{
public:
  DataPacket() : data_ {}, checkSum_ { 0 }, serverId_ { 0 }
  {
    std::cout << "DataPacket() " << std::endl;
  }

  DataPacket( std::string const& data, size_t serverId )
      : data_ { data }, checkSum_ { calcCheckSum( data ) }, serverId_ { serverId }
  {
    std::cout << "DataPacket(const std::string &data, size_t serverId)" << std::endl;
  }

  std::string const& getData() const
  {
    return data_;
  }
  void setData( std::string const& data )
  {
    data_ = data;
    checkSum_ = calcCheckSum( data );
  }
  size_t getCheckSum() const
  {
    return checkSum_;
  }

  void setServerId( size_t id )
  {
    serverId_ = id;
  }
  size_t getServerId() const
  {
    return serverId_;
  }

private:
  std::string data_;
  size_t checkSum_;
  size_t serverId_;
};

class DebugDataPacket : public DataPacket
{
public:
  using DataPacket::DataPacket; // Inheriting constructors

  void DebugPrint( std::ostream& os )
  {
    os << getData() << ", " << getCheckSum() << '\n';
  }
};

int main()
{
  DebugDataPacket hello { "hello!", 404 };
  hello.DebugPrint( std::cout );
}
