# C++ Initialization Story
	1. Local Variables and Simple Types
	2. Initialization With Constructors
	3. Copy and Move Operations
	4. Delegating and Inheriting Constructors
	5. Destructors
	6. Type Deduction and Initialization
	8. Non-Static Data Member Initialization(NSDMI)
	9. Containers as Data Members
	10. Non-regular Data Members
	11. Non-local objects
	12. Aggregates and Designated Initializers in C++20
